import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/weather_app.dart';

import 'injection/bloc_factory.dart';
import 'injection/modules.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  GetIt injector = GetIt.instance;
  registerModules(injector);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(Provider<BlocFactory>(
        create: (context) => BlocFactory(injector: injector),
        child: WeatherApp()));
  });
}
