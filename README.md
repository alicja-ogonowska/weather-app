# weather_app
Simple Flutter weather app created by Alicja Ogonowska

## Requirements
- Flutter 2.0.0+
- Dart 2.12.0+

## Getting Started
To generate json (de-)serializers for model classes, run command:
````
flutter packages pub run build_runner build
````

## Task
Given task is to prepare basic weather app using Flutter framework.

Application uses public API (https://www.metaweather.com/api/) to fetch data for a given location (hardcoded location is Berlin, Germany, with woeid of 638242) and display weather condition for the current day and a 5-days forecast.

User can display detailed weather for each of the 6 days and choose between two temperature units - Celsius and Fahrenheit.

Pull-to-refresh was implemented. Selected date is persisted across data refreshes and unit changes. If an error occurs while fetching the data, user can retry the request.


Application supports only portrait mode. To make it more comfortable for the user, all the other orientations were blocked.

If there was a real need for supporting the landscape mode as well, then an OrientationBuilder with a separate content layout would be used.

## Approach
For the state management I used BLoC implemented with flutter_bloc package. There are two main blocs in the application:

 - weather_bloc - esponsible for accquiring weather data from repository

 - weather_details_bloc - responsible for handling selected day and unit


Get It was used for Dependency Injection.

Test folder contains tests for the blocs, as well for the temperature converting util.


